unit u_sxsyn;

{$I u_defines.inc}

interface

uses
  Classes,
  SynEditHighlighter, SynEditHighlighterFoldBase, SynEditTypes,
  u_common;

type

  KeywordMatch = record
  private

  {
        rendered on 2024-Feb-21 05:21:19.9635608 by IsItThere.
         - PRNG seed: 6574
         - map length: 128
         - case sensitive: true
  }

  const fWords: array [0..127] of string =
    ('label', 'switch', '', 'goto', '', '', '', 'char', 'delete', 'var', '', '', '', '', '', 'apply', '', '', 'on', '', '', 'from', 'alias', '', '', '', 'static', 'super', 'overload', 'usize', '', 'false', '', 'template', 'continue', '', '', 'this', 'new', 'in', 'foreach', 'throw', 'u8', '', '', '', '', 'const', 'break', 'union', '', 'ssize', 'true', '', 'class', '', 'unit', 'version', '', '', '', '', 'asm', '', 's8', 'echo', '', '', '', '', '', 'u16', '', '', '', 'function', 'protection', 'enum', '', 'while', 'defer', '', '', 'return', 'if', '', '', 'u32', '', '', '', 'else', '', 's16', 'import', '', 'auto', 'try', 'bool', '', '', '', 'finally', '', 'null', 'u64', 'f32', '', '', 's32', '', 'struct', '', '', '', 'assert', 'do', '', '', '', '', '', '', '', 'f64', 'with', '', 's64');

  const fFilled: array [0..127] of boolean =
    (true, true, false, true, false, false, false, true, true, true, false, false, false, false, false, true, false, false, true, false, false, true, true, false, false, false, true, true, true, true, false, true, false, true, true, false, false, true, true, true, true, true, true, false, false, false, false, true, true, true, false, true, true, false, true, false, true, true, false, false, false, false, true, false, true, true, false, false, false, false, false, true, false, false, false, true, true, true, false, true, true, false, false, true, true, false, false, true, false, false, false, true, false, true, true, false, true, true, true, false, false, false, true, false, true, true, true, false, false, true, false, true, false, false, false, true, true, false, false, false, false, false, false, false, true, true, false, true);

  const fCoefficients: array [0..255] of Byte =
    (37, 236, 86, 118, 73, 206, 4, 48, 111, 209, 73, 137, 75, 157, 197, 152, 28, 40, 164, 170, 89, 179, 110, 152, 34, 12, 168, 23, 76, 66, 91, 217, 64, 82, 153, 12, 185, 137, 120, 56, 55, 132, 75, 141, 42, 119, 214, 156, 103, 102, 23, 72, 8, 181, 233, 248, 50, 191, 134, 63, 153, 91, 211, 31, 248, 110, 52, 135, 183, 250, 187, 30, 201, 228, 26, 255, 211, 174, 96, 188, 196, 206, 170, 218, 210, 19, 147, 137, 85, 186, 122, 214, 221, 218, 67, 147, 91, 27, 241, 246, 192, 226, 11, 2, 53, 201, 105, 1, 137, 149, 94, 180, 242, 169, 65, 14, 25, 248, 173, 230, 80, 135, 108, 41, 129, 206, 58, 41, 231, 66, 9, 230, 66, 181, 237, 77, 132, 143, 61, 65, 125, 248, 77, 147, 107, 74, 69, 83, 82, 217, 61, 119, 70, 229, 222, 170, 236, 100, 226, 92, 167, 163, 84, 207, 175, 75, 43, 27, 105, 100, 137, 0, 50, 34, 66, 135, 19, 75, 41, 113, 105, 88, 81, 86, 255, 235, 167, 229, 20, 181, 28, 233, 251, 234, 32, 117, 113, 48, 178, 193, 180, 111, 116, 38, 1, 16, 93, 108, 26, 83, 202, 163, 49, 177, 142, 229, 120, 149, 99, 182, 110, 136, 118, 198, 153, 237, 92, 131, 160, 58, 248, 253, 183, 192, 200, 200, 172, 192, 213, 137, 2, 208, 24, 94, 17, 41, 66, 9, 196, 119, 30, 99, 11, 28, 148, 180);

    class function hash(const w: string): Word; static;
  public
    class function contains(const w: string): boolean; static;
  end;

  TTokenKind = (tkNone, tkError, tkCommt, tkIdent, tkKeywd, tkStrng, tkBlank, tkSymbl, tkNumbr, tkAttri);

  TRangeKind = (rkNone, rkString1, rkString2, rkBlockCom1, rkAttrib);

  TSynSxSynRange = class (TSynCustomHighlighterRange)
  private
    rangeKind           : TRangeKind;
  public
    procedure Assign(source: TSynCustomHighlighterRange); override;
    function Compare(range: TSynCustomHighlighterRange): integer; override;
    procedure Clear; override;
    procedure copyFrom(source: TSynSxSynRange);
  end;

  TSynSxSyn = class (TSynCustomFoldHighlighter)
  private
    fWhiteAttrib: TSynHighlighterAttributes;
    fNumbrAttrib: TSynHighlighterAttributes;
    fSymblAttrib: TSynHighlighterAttributes;
    fIdentAttrib: TSynHighlighterAttributes;
    fCommtAttrib: TSynHighlighterAttributes;
    fStrngAttrib: TSynHighlighterAttributes;
    fKeywdAttrib: TSynHighlighterAttributes;
    fAttriAttrib: TSynHighlighterAttributes;
    fErrorAttrib: TSynHighlighterAttributes;

    fLineBuf: string;
    fTokStart, fTokStop: Integer;
    fTokKind: TTokenKind;
    fCurrRange: TSynSxSynRange;
    fLineNum: integer;

    fAttribLut: array[TTokenKind] of TSynHighlighterAttributes;

    procedure setWhiteAttrib(value: TSynHighlighterAttributes);
    procedure setNumbrAttrib(value: TSynHighlighterAttributes);
    procedure setSymblAttrib(value: TSynHighlighterAttributes);
    procedure setIdentAttrib(value: TSynHighlighterAttributes);
    procedure setCommtAttrib(value: TSynHighlighterAttributes);
    procedure setStrngAttrib(value: TSynHighlighterAttributes);
    procedure setKeywdAttrib(value: TSynHighlighterAttributes);
    procedure setAttriAttrib(value: TSynHighlighterAttributes);
    procedure setErrorAttrib(value: TSynHighlighterAttributes);

    function safeLookupChar(): PChar;
    function canLookup2Char(): boolean;
    procedure lexOpAndOpEqual();
    procedure lexOpAndOpOpAndOpEqual(const op: char);
    procedure lexOpAndOpOpAndOpEqualAndOpOpEqual(const op: char);
    procedure lexAssEquOrLambda();
    procedure lexHexLiteral();
    procedure lexBinLiteral();
    procedure lexIntLiteral();
    procedure lexFloatingLiteralFractionalPart();
    procedure lexExponent();
    procedure lexStringLiteral();
    procedure lexCharLiteral();
    procedure lexRawStringLiteral();
    procedure lexLineComment();
    procedure lexStarComment();
    procedure lexIdentifier();

  protected
    function GetRangeClass: TSynCustomHighlighterRangeClass; override;
    function GetIdentChars: TSynIdentChars; override;

  public

    constructor create(aOwner: TComponent); override;
    destructor destroy; override;

    procedure GetTokenEx(out TokenStart: PChar; out TokenLength: integer); override;
    function GetDefaultAttribute(Index: integer): TSynHighlighterAttributes; override;
    procedure setLine(const NewValue: string; LineNumber: Integer); override;
    procedure next; override;
    function  GetTokenAttribute: TSynHighlighterAttributes; override;
    function GetToken: string; override;
    function GetTokenKind: integer; override;
    function GetTokenPos: Integer; override;
    function GetEol: Boolean; override;
    procedure SetRange(value: Pointer); override;
    procedure ResetRange; override;
    function GetRange: Pointer; override;

    property whites:      TSynHighlighterAttributes read fWhiteAttrib write setWhiteAttrib stored true;
    property numbers:     TSynHighlighterAttributes read fNumbrAttrib write setNumbrAttrib stored true;
    property symbols:     TSynHighlighterAttributes read fSymblAttrib write setSymblAttrib stored true;
    property identifiers: TSynHighlighterAttributes read fIdentAttrib write setIdentAttrib stored true;
    property comments:    TSynHighlighterAttributes read fCommtAttrib write setCommtAttrib stored true;
    property strings:     TSynHighlighterAttributes read fStrngAttrib write setStrngAttrib stored true;
    property keywords:    TSynHighlighterAttributes read fKeywdAttrib write setKeywdAttrib stored true;
    property attributes:  TSynHighlighterAttributes read fAttriAttrib write setAttriAttrib stored true;
    property errors:      TSynHighlighterAttributes read fErrorAttrib write setErrorAttrib stored true;

  end;

implementation

{$PUSH}{$R-}
class function KeywordMatch.hash(const w: string): Word;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to length(w) do
    Result += fCoefficients[Byte(w[i])];
  Result := Result mod 128;
end;

class function KeywordMatch.contains(const w: string): boolean;
var
  h: Word;
begin
  result := false;
  h := hash(w);
  if fFilled[h] then
    result := fWords[h] = w;
end;
{$POP}

procedure TSynSxSynRange.Assign(source: TSynCustomHighlighterRange);
var
  rng: TSynSxSynRange;
begin
  inherited;
  if source is TSynSxSynRange then
  begin
    rng       := TSynSxSynRange(source);
    rangeKind := rng.rangeKind;
  end;
end;

function TSynSxSynRange.Compare(range: TSynCustomHighlighterRange): integer;
begin
  result := inherited Compare(range);
  if (range is TSynSxSynRange) and (rangeKind = TSynSxSynRange(range).rangeKind) then
    result := 1;
end;

procedure TSynSxSynRange.Clear;
begin
  inherited;
  rangeKind := TRangeKind.rkNone;
end;

procedure TSynSxSynRange.copyFrom(source: TSynSxSynRange);
begin
  if source.isAssigned then
    rangeKind := source.rangeKind;
end;

constructor TSynSxSyn.create(aOwner: TComponent);
begin
	inherited create(aOwner);

  DefaultFilter:= 'STYX source|*.sx|STYX archive|*.sar';

  WordBreakChars := WordBreakChars - ['@'];

  fWhiteAttrib := TSynHighlighterAttributes.Create('White','White');
  fNumbrAttrib := TSynHighlighterAttributes.Create('Numbr','Numbr');
  fSymblAttrib := TSynHighlighterAttributes.Create('Symbl','Symbl');
  fIdentAttrib := TSynHighlighterAttributes.Create('Ident','Ident');
  fCommtAttrib := TSynHighlighterAttributes.Create('Commt','Commt');
  fStrngAttrib := TSynHighlighterAttributes.Create('Strng','Strng');
  fKeywdAttrib := TSynHighlighterAttributes.Create('Keywd','Keywd');
  fAttriAttrib := TSynHighlighterAttributes.Create('Attri','Attri');
  fErrorAttrib := TSynHighlighterAttributes.Create('Error','Error');

  AddAttribute(fWhiteAttrib);
  AddAttribute(fNumbrAttrib);
  AddAttribute(fSymblAttrib);
  AddAttribute(fIdentAttrib);
  AddAttribute(fCommtAttrib);
  AddAttribute(fStrngAttrib);
  AddAttribute(fKeywdAttrib);
  AddAttribute(fAttriAttrib);
  AddAttribute(fErrorAttrib);

  fAttribLut[TTokenKind.tkident] := fIdentAttrib;
  fAttribLut[TTokenKind.tkBlank] := fWhiteAttrib;
  fAttribLut[TTokenKind.tkCommt] := fCommtAttrib;
  fAttribLut[TTokenKind.tkKeywd] := fKeywdAttrib;
  fAttribLut[TTokenKind.tkNumbr] := fNumbrAttrib;
  fAttribLut[TTokenKind.tkStrng] := fStrngAttrib;
  fAttribLut[TTokenKind.tksymbl] := fSymblAttrib;
  fAttribLut[TTokenKind.tkAttri] := fAttriAttrib;
  fAttribLut[TTokenKind.tkError] := fErrorAttrib;

end;

destructor TSynSxSyn.destroy;
begin
  fCurrRange.Free;
  inherited;
end;

function TSynSxSyn.GetRangeClass: TSynCustomHighlighterRangeClass;
begin
  result := TSynSxSynRange;
end;

function TSynSxSyn.GetIdentChars: TSynIdentChars;
begin
  result := ['_', 'A'..'Z', 'a'..'z', '0'..'9'];
end;

procedure TSynSxSyn.setWhiteAttrib(value: TSynHighlighterAttributes);
begin
  fWhiteAttrib.Assign(value);
end;

procedure TSynSxSyn.setNumbrAttrib(value: TSynHighlighterAttributes);
begin
  fNumbrAttrib.Assign(value);
end;

procedure TSynSxSyn.setSymblAttrib(value: TSynHighlighterAttributes);
begin
  fSymblAttrib.Assign(value);
end;

procedure TSynSxSyn.setIdentAttrib(value: TSynHighlighterAttributes);
begin
  fIdentAttrib.Assign(value);
end;

procedure TSynSxSyn.setCommtAttrib(value: TSynHighlighterAttributes);
begin
  fCommtAttrib.Assign(value);
end;

procedure TSynSxSyn.setStrngAttrib(value: TSynHighlighterAttributes);
begin
  fStrngAttrib.Assign(value);
end;

procedure TSynSxSyn.setKeywdAttrib(value: TSynHighlighterAttributes);
begin
  fKeywdAttrib.Assign(value);
end;

procedure TSynSxSyn.setAttriAttrib(value: TSynHighlighterAttributes);
begin
  fAttriAttrib.Assign(value);
end;

procedure TSynSxSyn.setErrorAttrib(value: TSynHighlighterAttributes);
begin
  fErrorAttrib.Assign(value);
end;

function TSynSxSyn.GetTokenAttribute: TSynHighlighterAttributes;
begin
  result := fAttribLut[fTokKind];
end;

procedure TSynSxSyn.SetRange(value: Pointer);
var
  stored: TSynSxSynRange;
begin
  inherited SetRange(value);
  stored := TSynSxSynRange(CodeFoldRange.RangeType);
  if fCurrRange.isAssigned and stored.isAssigned then
    fCurrRange.copyFrom(stored);
end;

function TSynSxSyn.GetRange: Pointer;
var
  stored: TSynSxSynRange;
begin
  stored := TSynSxSynRange(inherited GetRange);
  if stored.isNotAssigned then
    stored := TSynSxSynRange.Create(nil);
  stored.copyFrom(fCurrRange);

  CodeFoldRange.RangeType := Pointer(stored);
  Result := inherited GetRange;
end;

procedure TSynSxSyn.ResetRange;
begin
  if fCurrRange.isNotAssigned then
    fCurrRange := TSynSxSynRange.Create(nil)
  else
    fCurrRange.Clear;
end;

procedure TSynSxSyn.setLine(const NewValue: string; LineNumber: Integer);
begin
  inherited;
  fLineBuf := NewValue;
  fLineNum := LineNumber;
  fTokStop := 1;
  next;
end;

function TSynSxSyn.GetEol: Boolean;
begin
  result :=  fTokKind = tkNone;
end;

function TSynSxSyn.GetTokenPos: Integer;
begin
  result := fTokStart - 1;
end;

function TSynSxSyn.GetToken: string;
begin
  result := copy(fLineBuf, FTokStart, fTokStop - FTokStart);
end;

procedure TSynSxSyn.GetTokenEx(out TokenStart: PChar; out TokenLength: integer);
begin
  TokenStart  := @fLineBuf[FTokStart];
  TokenLength := fTokStop - FTokStart;
end;

function TSynSxSyn.GetDefaultAttribute(Index: integer): TSynHighlighterAttributes;
begin
  result := nil;
end;

function TSynSxSyn.GetTokenKind: integer;
begin
  Result := Integer(fTokKind);
end;

function TSynSxSyn.safeLookupChar: PChar;
begin
  if fTokStop >= length(fLineBuf) then
    result := nil
  else result := @fLineBuf[fTokStop + 1];
end;

function TSynSxSyn.canLookup2Char(): boolean;
begin
  result := fTokStop + 2 <= length(fLineBuf);
end;

procedure TSynSxSyn.lexOpAndOpEqual();
var
  nextPChar: PChar;
  nextChar: char;
begin
  fTokKind  := TTokenKind.tkSymbl;
  nextPChar := safeLookupChar();
  if nextPChar <> nil then
  begin
    nextChar := nextPChar^;
    if nextChar = '=' then
      fTokStop += 1;
  end;
  fTokStop  += 1;
end;

procedure TSynSxSyn.lexOpAndOpOpAndOpEqual(const op: char);
var
  nextPChar: PChar;
  nextChar: char;
begin
  fTokKind  := TTokenKind.tkSymbl;
  nextPChar := safeLookupChar();
  if nextPChar <> nil then
  begin
    nextChar := nextPChar^;
    if (nextChar = op) or (nextChar = '=') then
      fTokStop += 1;
  end;
  fTokStop  += 1;
end;

procedure TSynSxSyn.lexOpAndOpOpAndOpEqualAndOpOpEqual(const op: char);
var
  nextPChar: PChar;
  nextChar: char;
  can2: boolean;
begin
  fTokKind  := TTokenKind.tkSymbl;
  can2      := canLookup2Char();
  nextPChar := safeLookupChar();
  // <<=
  if can2 and (fLineBuf[fTokStop + 1] = op) and (fLineBuf[fTokStop+2] = '=') then
    fTokStop  += 2
  else if nextPChar <> nil then
  begin
    nextChar := nextPChar^;
    // << or <=
    if (nextChar = op) or (nextChar = '=') then
      fTokStop += 1;
  end;
  fTokStop  += 1;
end;

procedure TSynSxSyn.lexAssEquOrLambda();
var
  nextPChar: PChar;
  nextChar: char;
begin
  fTokKind  := TTokenKind.tkSymbl;
  nextPChar := safeLookupChar();
  if nextPChar <> nil then
  begin
    nextChar := nextPChar^;
    if (nextChar = '=') or (nextChar = '>') then
      fTokStop += 1;
  end;
  fTokStop  += 1;
end;

procedure TSynSxSyn.lexIntLiteral();
var
  nextPChar: PChar;
  nextChar: char;
begin
  fTokKind:=TTokenKind.tkNumbr;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '0'..'9', '_':
      begin
        fTokStop += 1;
        continue;
      end;
      '.':
      begin
        nextPChar := safeLookupChar();
        if nextPChar <> nil then
        begin
          nextChar := nextPChar^;
          if nextChar in ['0' .. '9'] then
          begin
            fTokStop += 1;
            lexFloatingLiteralFractionalPart();
            exit;
          end;
        end;
        break;
      end;
    end;
    break;
  end;
end;

procedure TSynSxSyn.lexFloatingLiteralFractionalPart();
begin
  fTokKind:=TTokenKind.tkNumbr;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '0'..'9', '_': fTokStop += 1;
      'e', 'E':
      begin
        lexExponent();
        exit;
      end
      else break;
    end;
  end;
end;

procedure TSynSxSyn.lexExponent();
begin
  fTokStop += 1;
  if fTokStop > fLineBuf.length then
  begin
    fTokKind:=TTokenKind.tkError;
    exit;
  end;
  if fLineBuf[fTokStop] in ['+', '-'] then
    fTokStop += 1;
  if fTokStop > fLineBuf.length then
  begin
    fTokKind:=TTokenKind.tkError;
    exit;
  end;
  while fTokStop <= fLineBuf.length do
  begin
    if fLineBuf[fTokStop] in ['0' .. '9'] then
    begin
      fTokStop += 1;
      continue;
    end else
      break;
  end;
end;

procedure TSynSxSyn.lexHexLiteral();
var
  firstChar: Boolean = false;
begin
  fTokStop += 2;
  fTokKind:=TTokenKind.tkNumbr;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '0'..'9', 'a'..'f', 'A'..'F', '_':
      begin
        if not firstChar and (fLineBuf[fTokStop] = '_') then
          fTokKind:=TTokenKind.tkError;
        fTokStop  += 1;
        firstChar := true;
        continue;
      end
      else while (fTokStop <= fLineBuf.length) and
        (fLineBuf[fTokStop] in ['g' .. 'z', 'G' .. 'Z']) do
      begin
        fTokKind := TTokenKind.tkError;
        fTokStop += 1;
      end;
    end;
    break;
  end;
end;

procedure TSynSxSyn.lexBinLiteral();
var
  firstChar: Boolean = false;
begin
  fTokStop += 2;
  fTokKind:=TTokenKind.tkNumbr;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '0', '1', '_':
      begin
        if not firstChar and (fLineBuf[fTokStop] = '_') then
          fTokKind:=TTokenKind.tkError;
        fTokStop  += 1;
        firstChar := true;
        continue;
      end;
      else while (fTokStop <= fLineBuf.length) and
        (fLineBuf[fTokStop] in ['2' .. '9', 'a' .. 'z', 'A' .. 'Z']) do
      begin
        fTokKind := TTokenKind.tkError;
        fTokStop += 1;
      end;
    end;
    break;
  end;
end;

procedure TSynSxSyn.lexCharLiteral();
begin
  fTokKind  := TTokenKind.tkStrng;
  fTokStop  += 1;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '\' : fTokStop += 2;
      #39 :
      begin
        fTokStop  += 1;
        exit;
      end
      else fTokStop += 1;
    end;
  end;
  fTokKind  := TTokenKind.tkError;
end;

procedure TSynSxSyn.lexStringLiteral();
var
  firstLine: Boolean;
  terminate: Boolean = false;
begin
  fTokKind  := TTokenKind.tkStrng;
  firstLine := fCurrRange.rangeKind = TRangeKind.rkNone;
  if firstLine then
    fTokStop  += 1;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '\' : fTokStop += 2;
      '"' :
      begin
        fTokStop  += 1;
        terminate := true;
        break;
      end
      else fTokStop += 1;
    end;
  end;
  if firstLine and not terminate then
    fCurrRange.rangeKind:= TRangeKind.rkString1
  else if (fCurrRange.rangeKind = TRangeKind.rkString1) and terminate then
    fCurrRange.rangeKind:= TRangeKind.rkNone;
end;

procedure TSynSxSyn.lexRawStringLiteral();
var
  firstLine: Boolean;
  terminate: Boolean = false;
begin
  fTokKind  := TTokenKind.tkStrng;
  firstLine := fCurrRange.rangeKind = TRangeKind.rkNone;
  if firstLine then
    fTokStop  += 1;
  while fTokStop <= fLineBuf.length do
  begin
    if fLineBuf[fTokStop] = '`' then
    begin
      fTokStop  += 1;
      terminate := true;
      break;
    end
    else fTokStop += 1;
  end;
  if firstLine and not terminate then
    fCurrRange.rangeKind:= TRangeKind.rkString2
  else if (fCurrRange.rangeKind = TRangeKind.rkString2) and terminate then
    fCurrRange.rangeKind:= TRangeKind.rkNone;
end;

procedure TSynSxSyn.lexLineComment();
begin
  fTokKind  := TTokenKind.tkCommt;
  fTokStop  := fLineBuf.length + 1;
end;

procedure TSynSxSyn.lexStarComment();
var
  firstLine: Boolean;
  terminate: Boolean = false;
begin
  fTokKind  := TTokenKind.tkCommt;
  firstLine := fCurrRange.rangeKind = TRangeKind.rkNone;
  while fTokStop <= fLineBuf.length do
  begin
    if fLineBuf[fTokStop] = '*' then
    begin
      fTokStop += 1;
      if (fTokStop <= fLineBuf.length) and (fLineBuf[fTokStop] = '/') then
      begin
        fTokStop += 1;
        terminate := true;
        break;
      end;
    end
    else fTokStop += 1;
  end;
  if firstLine and not terminate then
    fCurrRange.rangeKind := TRangeKind.rkBlockCom1
  else if (fCurrRange.rangeKind = TRangeKind.rkBlockCom1) and terminate then
    fCurrRange.rangeKind := TRangeKind.rkNone;
end;

procedure TSynSxSyn.lexIdentifier();
var
  dollar: boolean;
  oneChr: boolean = false;
begin
  fTokKind  := TTokenKind.tkIdent;
  dollar    := fLineBuf[fTokStop] = '$';
  if dollar then
    fTokStop += 1;
  while fTokStop <= fLineBuf.length do
  begin
    case fLineBuf[fTokStop] of
      '_', 'a'..'z', 'A'..'Z', '0'..'9':
      begin
        oneChr    := true;
        fTokStop  += 1;
        continue;
      end;
      else break;
    end;
  end;
  if dollar and not oneChr then
    fTokKind := TTokenKind.tkSymbl
  else
  begin
    if KeywordMatch.contains(GetToken()) then
      fTokKind := TTokenKind.tkKeywd;
  end;
end;

procedure TSynSxSyn.next;
var
  llen: integer;
  nextPChar: PChar;
  nextChar: char;
begin
  fTokKind  := tkNone;
  fTokStart := fTokStop;
  llen := length(fLineBuf);

  // EOL
  if fTokStop > llen then
    exit;

  // continue partial multi-line ranges
  if fCurrRange.isNotAssigned then
    fCurrRange := TSynSxSynRange.Create(nil)
  else case fCurrRange.rangeKind of
    TRangeKind.rkString1:   begin lexStringLiteral(); exit;     end;
    TRangeKind.rkString2:   begin lexRawStringLiteral(); exit;  end;
    TRangeKind.rkBlockCom1: begin lexStarComment(); exit;       end;
  end;

  // special lines
  if (fLineBuf.length > 1) then
  begin
    // she bang
    if (fLineNum = 0) and (fLineBuf[1..2] = '#!') then
    begin
      lexLineComment();
      exit;
    end
    // "§" of SAR
    else if (fTokStart = 1) and (fLineBuf[1..2] = #194#167) then
    begin
      lexLineComment();
      exit;
    end;
  end;

  case fLineBuf[fTokStop] of
    #0 .. #10, #13, #32:
    begin
      fTokStop += 1;
      fTokKind := TTokenKind.tkBlank;
    end;
    // `//comment` `/*comment` `/=` `/`
    '/':
    begin
      fTokKind := TTokenKind.tkSymbl;
      fTokStop += 1;
      if (fTokStop <= llen) then
        case fLineBuf[fTokStop] of
        '/' : lexLineComment();
        '*' : begin fTokStop  += 1; lexStarComment(); end;
        '=' : fTokStop += 1;
      end;
    end;
    // `_ident`  `$kwIdent`
    'a'..'z', 'A'..'Z', '_', '$': lexIdentifier();
    // `0x...` `0b...` `012...` `0.12...` `012...`
    '0':
    begin
      nextPChar := safeLookupChar();
      if nextPChar <> nil then
      begin
        nextChar := nextPChar^;
        if (nextChar = 'x') or (nextChar = 'X') then
          lexHexLiteral()
        else if (nextChar = 'b') or (nextChar = 'B') then
          lexBinLiteral()
        else lexIntLiteral();
      end
      else lexIntLiteral();
    end;
    // number
    '1' .. '9' : lexIntLiteral();
    // char
    #39 : lexCharLiteral();
    // "string"
    '"': lexStringLiteral();
    // `string`
    '`': lexRawStringLiteral();
    // `-` `-=` `--`
    '-': lexOpAndOpOpAndOpEqual('-');
    // `&` `&=` `&&`
    '&': lexOpAndOpOpAndOpEqual('&');
    // `|` `|=` `||`
    '|': lexOpAndOpOpAndOpEqual('|');
    // `+` `+=` `++`
    '+': lexOpAndOpOpAndOpEqual('+');
    // `*` `*=` `%` `%=` `^` `^=` `~` `~=`  `!` `!=`
    '*', '%', '^', '!', '~', '?': lexOpAndOpEqual();
    // `<` `<<` `<=` `<<=` `>` `>>` `>=` `>>=`
    '<': lexOpAndOpOpAndOpEqualAndOpOpEqual('<');
    '>': lexOpAndOpOpAndOpEqualAndOpOpEqual('>');
    // `=`, `==`, `=>`
    '=': lexAssEquOrLambda();
    '.':
    begin
      nextPChar := safeLookupChar();
      fTokKind  := TTokenKind.tkSymbl;
      if (nextPChar <> nil) and (nextPChar^ = '=') then
          fTokStop  += 2
      else
          fTokStop  += 1;
    end;
    '(', ')', ',', ':' , '[', ']', ';' :
    begin
      fTokKind  := TTokenKind.tkSymbl;
      fTokStop  += 1;
    end;
    '@':
    begin
      fTokKind  := TTokenKind.tkError;
      fTokStop  += 1;
      if fTokStop <= llen then
      begin
        lexIdentifier();
        fTokKind := TTokenKind.tkAttri;
      end;
    end;
    '{':
    begin
      fTokKind  := TTokenKind.tkSymbl;
      fTokStop  += 1;
      StartCodeFoldBlock();
    end;
    '}':
    begin
      fTokKind  := TTokenKind.tkSymbl;
      fTokStop  += 1;
      EndCodeFoldBlock();
    end
    else begin
      fTokStop  += 1;
      fTokKind  := TTokenKind.tkError;
    end;
  end;
end;

end.

