unit u_dlangmaps;

{$I u_defines.inc}

interface


type

  (**
   * Perfect static hash-map that detects the D2 "special" keywords such as
   * __LINE__ or __FILE__.
   *)
  specialKeywordsMap = record
  private
    const fWords: array [0..15] of string =
    (
      '__VERSION__', '', '__FILE_FULL_PATH__', '__TIME__', '__FILE__', '__VENDOR__',
      '', '__DATE__', '__FUNCTION__', '__LINE__', '__EOF__', '__MODULE__',
      '__PRETTY_FUNCTION__', '', '', '__TIMESTAMP__'
    );
    const fHasEntry: array [0..15] of boolean =
    (
      true, false, true, true, true, true, false, true, true, true, true, true,
      true, false, false, true
    );
    const fCoeffs: array[0..255] of Byte =
    (
      162, 105, 225, 180, 180, 12, 125, 73, 237, 109, 3, 67, 160, 192, 35, 42,
      131, 170, 41, 106, 103, 53, 105, 74, 29, 64, 247, 248, 184, 146, 172, 142,
      239, 232, 158, 168, 29, 243, 40, 241, 255, 85, 184, 38, 44, 242, 193, 222,
      86, 131, 181, 101, 161, 209, 115, 124, 91, 118, 188, 67, 172, 115, 24, 221,
      142, 99, 17, 30, 231, 80, 185, 182, 185, 55, 4, 23, 152, 63, 126, 37, 158,
      36, 28, 235, 65, 220, 243, 62, 169, 129, 127, 76, 149, 232, 21, 119, 134,
      144, 20, 89, 103, 65, 109, 12, 95, 200, 41, 14, 52, 25, 56, 228, 4, 227,
      86, 113, 77, 158, 46, 246, 90, 25, 210, 214, 149, 219, 219, 27, 95, 203,
      43, 21, 191, 94, 216, 113, 100, 222, 245, 224, 127, 174, 214, 44, 78, 89,
      213, 184, 73, 77, 236, 131, 46, 90, 58, 171, 34, 215, 201, 104, 138, 251,
      54, 103, 75, 235, 12, 149, 49, 19, 128, 72, 138, 224, 73, 174, 151, 50,
      152, 32, 135, 238, 132, 34, 3, 230, 201, 166, 31, 119, 50, 155, 125, 103,
      133, 250, 253, 218, 48, 167, 207, 107, 235, 53, 214, 213, 49, 8, 13, 247,
      37, 251, 21, 43, 34, 108, 162, 160, 133, 199, 169, 218, 189, 1, 128, 17,
      67, 186, 55, 2, 23, 23, 133, 114, 240, 176, 124, 127, 217, 231, 129, 220,
      250, 17, 136, 92, 191, 172, 16, 137, 23, 109, 37, 191, 74, 218
    );
    class function hash(const w: string): Byte; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  public
    class function match(const w: string): boolean; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  end;

  (**
   * Perfect static hash-map that detects the 'straight' D2 keywords plus a few
   * exception for the library types related to the strings and registry-wide integers.
   *)
  keywordsMap = record
  private
  {
        rendered on 2021-Apr-14 18:49:17.621124 by IsItThere.
         - PRNG seed: 0
         - map length: 512
         - case sensitive: true
  }

  const fWords: array [0..511] of string =
    ('', '', '', '', '', '', 'scope', '', '', '', '', 'this', 'ucent', 'delegate', 'size_t', '', '', 'ulong', 'nothrow', '', '', 'bool', '', '', '', '', 'debug', '', '', 'class', '', '', 'function', '', '', 'uint', 'private', '', 'for', '', '', '', 'false', 'deprecated', 'mixin', '', 'while', '', '', 'with', '', '', '', 'true', '', '', '', '', '', 'foreach_reverse', '', '', '', 'real', '', '', 'align', '', '', '__vector', '', '', '', '', 'pragma', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'module', '', '', '', '', '', '', '', '', '', '', '', '', '', 'interface', '', '', '', '', '', '', '', '', 'foreach', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'auto', '', 'float', '', 'typeid', 'else', '', 'string', 'creal', 'inout', '', '', 'pure', '', '', '', '', '', '', '', '', '', '', '', 'export', '', '', '', '', '', 'ubyte', 'double', '', '', '', '', '', '', '', 'do', '', '', '', 'union', '', '', '', '', '', 'static', '', 'final', 'ireal', '', '', '', '', '', 'is', '', '', 'immutable', '', 'switch', '', '', '', 'assert', '', '', '', '', 'dstring', '', '__traits', '', '', '', '', '', 'new', 'super', '', '', '', '', '', '', 'typeof', '', '', 'catch', 'short', '', 'alias', '', '', '', 'delete', 'in', '', '', '', '', '', 'cfloat', '', '', '', 'abstract', '', '', '', '__gshared', 'wstring', 'break', '', '', '', 'unittest', '', '', 'struct', '', '', '', '', '', '', '', 'throw', '', '', '', 'cdouble', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ifloat', 'ptrdiff_t', 'template', 'cent', 'if', '', '', '', 'long', '', '', '', '', '', '', '', '', 'char', '', '', '', 'default', '', '', '', '', 'int', '', '', 'idouble', 'volatile', '', '', '', '', 'version', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'goto', '', '', '', '', '', 'package', '__parameters', '', '', '', '', '', '', '', 'shared', '', '', '', '', '', '', '', '', '', '', '', '', 'dchar', '', '', '', '', '', '', '', '', '', '', '', 'continue', '', '', '', '', '', '', '', '', 'synchronized', '', '', '', 'public', '', '', 'return', '', '', '', '', 'out', '', 'import', '', '', '', 'null', 'protected', '', 'wchar', '', 'finally', '', '', '', 'invariant', '', 'ref', '', 'case', '', '', '', '', '', '', '', '', '', 'asm', '', '', '', '', '', '', '', '', '', '', '', '', 'ushort', 'void', '', '', '', '', '', '', '', '', '', '', '', '', '', 'enum', '', '', '', '', '', '', 'byte', '', '', '', 'const', '', 'lazy', '', '', '', 'cast', '', '', '', 'extern', '', 'try', 'override', '', '', '', '', '', '', '', '');

  const fHasEntry: array [0..511] of boolean =
    (false, false, false, false, false, false, true, false, false, false, false, true, true, true, true, false, false, true, true, false, false, true, false, false, false, false, true, false, false, true, false, false, true, false, false, true, true, false, true, false, false, false, true, true, true, false, true, false, false, true, false, false, false, true, false, false, false, false, false, true, false, false, false, true, false, false, true, false, false, true, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, true, true, false, true, true, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, true, false, false, false, false, false, false, false, true, false, false, false, true, false, false, false, false, false, true, false, true, true, false, false, false, false, false, true, false, false, true, false, true, false, false, false, true, false, false, false, false, true, false, true, false, false, false, false, false, true, true, false, false, false, false, false, false, true, false, false, true, true, false, true, false, false, false, true, true, false, false, false, false, false, true, false, false, false, true, false, false, false, true, true, true, false, false, false, true, false, false, true, false, false, false, false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, true, false, false, false, false, true, false, false, true, true, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, true, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, true, false, false, true, false, false, false, false, true, false, true, false, false, false, true, true, false, true, false, true, false, false, false, true, false, true, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, true, false, false, false, true, false, true, false, false, false, true, false, false, false, true, false, true, true, false, false, false, false, false, false, false, false);

  const fCoeffs: array [0..255] of Byte =
    (38, 33, 180, 0, 185, 246, 108, 140, 101, 70, 225, 36, 169, 241, 207, 131, 8, 145, 253, 246, 120, 51, 192, 218, 141, 3, 128, 13, 153, 122, 147, 180, 155, 253, 108, 112, 81, 69, 216, 179, 91, 136, 140, 39, 226, 59, 35, 105, 3, 111, 139, 247, 35, 246, 8, 124, 74, 191, 207, 221, 168, 208, 182, 40, 125, 146, 113, 139, 166, 251, 161, 145, 87, 99, 57, 74, 153, 249, 60, 234, 254, 164, 213, 146, 164, 14, 30, 121, 56, 203, 76, 11, 44, 178, 124, 193, 12, 248, 217, 103, 65, 21, 164, 21, 231, 147, 173, 45, 52, 134, 110, 132, 193, 209, 254, 69, 76, 214, 121, 107, 18, 172, 20, 89, 137, 103, 6, 153, 190, 154, 210, 136, 72, 198, 124, 133, 215, 154, 155, 198, 121, 208, 215, 184, 239, 210, 214, 130, 231, 158, 210, 177, 16, 39, 121, 200, 101, 187, 168, 82, 237, 94, 101, 118, 255, 207, 85, 168, 217, 106, 177, 5, 88, 246, 217, 77, 204, 40, 208, 142, 96, 217, 123, 31, 20, 237, 212, 146, 9, 168, 103, 214, 167, 134, 75, 204, 86, 62, 164, 83, 131, 236, 14, 131, 31, 66, 95, 70, 166, 134, 47, 8, 171, 76, 79, 136, 237, 10, 178, 22, 235, 255, 228, 205, 162, 40, 103, 57, 169, 153, 164, 128, 28, 5, 216, 168, 35, 121, 33, 96, 39, 72, 230, 91, 54, 134, 223, 59, 5, 166, 146, 71, 188, 129, 179, 117);
    class function hash(const w: string): Word; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  public
    class function match(const w: string): boolean; static; {$IFNDEF DEBUG}inline;{$ENDIF}
  end;


implementation

{$IFDEF DEBUG}{$PUSH}{$R-}{$ENDIF}
class function specialKeywordsMap.hash(const w: string): Byte;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to length(w) do
    Result += fCoeffs[Byte(w[i])];
  Result := Result and $F;
end;
{$IFDEF DEBUG}{$POP}{$ENDIF}

class function specialKeywordsMap.match(const w: string): boolean;
var
  h: Byte;
begin
  result := false;
  if (length(w) < 7) or (length(w) > 19) then
    exit;
  h := hash(w);
  if fHasEntry[h] then
    result := fWords[h] = w;
end;

{$IFDEF DEBUG}{$PUSH}{$R-}{$ENDIF}
class function keywordsMap.hash(const w: string): Word;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to length(w) do
    Result += fCoeffs[Byte(w[i])];
  Result := Result and $1FF;
end;
{$IFDEF DEBUG}{$POP}{$ENDIF}

class function keywordsMap.match(const w: string): boolean;
var
  h: Word;
begin
  result := false;
  if (length(w) < 2) or (length(w) > 15) then
    exit;
  h := hash(w);
  if fHasEntry[h] then
    result := fWords[h] = w;
end;

end.

